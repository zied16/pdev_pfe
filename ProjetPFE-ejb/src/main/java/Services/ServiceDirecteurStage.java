package Services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.w3c.tidy.Tidy;

import Entity.AnneeUniversitaire;
import Entity.Categorie;
import Entity.CategorieFiche;
import Entity.CategorieFichePK;
import Entity.Entreprise;
import Entity.Etudiant;
import Entity.FichePFE;
import Interface.DirecteurStageRemote;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
@Stateless
@LocalBean
public class ServiceDirecteurStage implements DirecteurStageRemote {

	@PersistenceContext
	EntityManager em;

	@Override
	public ArrayList<Etudiant> AfficherListEtudiant(int idsite) {
		//TypedQuery<Etudiant> query = em.createQuery(
		Query query = em.createQuery(
				"SELECT e FROM etudiant e JOIN e.departements  d join d.site s"
				+ "  WHERE s.id=:idsite",
				Etudiant.class);
		query.setParameter("idsite", idsite);
		 System.out.println(query.getResultList());
		ArrayList<Etudiant> e =new ArrayList<>();
		
		e=(ArrayList<Etudiant>) query.getResultList();
		
		return e;
		
	}

	@Override
	public double PourcentageStageEtranger(String  tunisie) {
		TypedQuery<Long> query = em.createQuery(
				"SELECT  COUNT(*)  FROM etudiant e JOIN e.entreprise  en "
				+ "  WHERE en.Pays=:tunisie "
				,Long.class);
		query.setParameter("tunisie", tunisie);
		TypedQuery<Long> query1 = em.createQuery(
				"select count (* )from etudiant  ",
				Long.class);
		//query.setParameter("employeid", employeId);
		double a =(1-((float)query.getSingleResult()/(float)query1.getSingleResult()))*100;
		return a;
		//return 0;
	}

	@Override
	public double PourcentageStageEtranger1(String pays) {
		
			TypedQuery<Long> query = em.createQuery(
					"SELECT  COUNT(*)  FROM Entreprise en  "
					+ "  WHERE en.Pays=:pays "
					,Long.class);
			query.setParameter("pays", pays);
			/*TypedQuery<Long> query1 = em.createQuery(
					"select count (* )from etudiant  ",
					Long.class);
			//query.setParameter("employeid", employeId);
			double a =(query.getSingleResult()/query1.getSingleResult())*100;*/
			double a =query.getSingleResult();
			return a;
		
	}

	@Override
	public long Nombreetudiantparsite(int idsite) {
		TypedQuery<Long> query = em.createQuery(
				"SELECT  COUNT(*)  FROM etudiant e join e.departements d join d.site s  "
				+ "  WHERE s.id=:idsite group by s.Lieu"
				,Long.class);
		query.setParameter("idsite", idsite);
		/*TypedQuery<Long> query1 = em.createQuery(
				"select count (* )from etudiant  ",
				Long.class);
		//query.setParameter("employeid", employeId);
		double a =(query.getSingleResult()/query1.getSingleResult())*100;*/
		long a =query.getSingleResult();
		return a;
		
	}

	@Override
	public ArrayList<Etudiant> listentrepriseetudiantesprit(String esprit) {
		
		Query query = em.createQuery(
				"SELECT  e FROM etudiant e"
				+ "    join e.departements d join "
				+ "d.site s join s.universite u "
				+ "  WHERE u.Nom=:esprit group by e.entreprise order by e.entreprise desc  ",
				Etudiant.class);
		
		query.setParameter("esprit", esprit);
ArrayList<Etudiant> ese =new ArrayList<>();
		
		ese=(ArrayList<Etudiant>) query.getResultList();
		
		
		return ese;
		
	}

	@Override
	public ArrayList<FichePFE> listercatégorie() {
		Query query = em.createQuery(
				"SELECT catégories FROM fichepfe Catégories",
				FichePFE.class);
		
		 System.out.println(query.getResultList());
		ArrayList<FichePFE> e =new ArrayList<>();
		
		e=(ArrayList<FichePFE>) query.getResultList();
		
		return e;
	}
	public ArrayList<Object[]>listercatégorieavec() {
		Query query = em.createQuery(
				//"SELECT  count(*) catégories FROM fichepfe Catégories group by catégories"
				"select Catégories as c ,  count(*) as n  FROM fichepfe f group by f.Catégories order by n  desc"
				);
		
		 System.out.println(query.getResultList());
		
		 ArrayList<Object[]> e =new ArrayList<>();
			
			e=(ArrayList<Object[]>) query.getResultList();
		
		return e;
	}

	@Override
	public double evolutionPourcentageStageEtranger(String tunisie,int annee) {
		TypedQuery<Long> query = em.createQuery(
				"SELECT  COUNT(*)  FROM etudiant e JOIN e.entreprise  en join  e.fiche f join f.year y "
				+ "  WHERE en.Pays=:tunisie and y.Annee=:annee "
				,Long.class);
		query.setParameter("tunisie", tunisie);
		query.setParameter("annee", annee);
		/*TypedQuery<Long> query1 = em.createQuery(
				"select count (* )from etudiant  ",
				Long.class);*/
		//query.setParameter("employeid", employeId);
		double a =(float)query.getSingleResult();/*/(float)query1.getSingleResult())*100;*/
		return a;
		//return 0;
	}

	@Override
	public long Nombreetudiantparsiteetparannee(int idsite, int year) {
		TypedQuery<Long> query = em.createQuery(
				"SELECT  COUNT(*)  FROM etudiant e join e.departements d join d.site s join  e.classe c join c.annee y "
				+ "  WHERE s.id=:idsite and y.Annee=:year "
				,Long.class);
		query.setParameter("idsite", idsite);
		query.setParameter("year", year);
		/*TypedQuery<Long> query1 = em.createQuery(
				"select count (* )from etudiant  ",
				Long.class);join  e.fiche f join f.year y
		//query.setParameter("employeid", employeId);
		double a =(query.getSingleResult()/query1.getSingleResult())*100;*/
		long a =(long)query.getSingleResult();
		return a;
		
	}

	@Override
	public void ajouterCategoriefiche(int idfiche, int idCategorie) {
		CategorieFichePK ch=new CategorieFichePK();
		ch.setIdCategorie(idCategorie);
		ch.setIdEmploye(idfiche);;
		//Categorie c = em.find(Categorie.class, idCategorie);
		//FichePFE f = em.find(FichePFE.class, idfiche);
	
		CategorieFiche choix = new CategorieFiche();
		choix.setIdCategoriefiche(ch);
		
		em.persist(choix);
		
		
	}

	@Override
	public ArrayList<CategorieFiche> listerficheavecsescatégorie() {
		
			TypedQuery<CategorieFiche> query = em.createQuery(
					//"SELECT  count(*) catégories FROM fichepfe Catégories group by catégories"
					"select f FROM CategorieFiche f "
					,CategorieFiche.class);
			//query.setParameter("id", id);
			
			 System.out.println(query.getResultList());
			
			 ArrayList<CategorieFiche> e =new ArrayList<>();
				
				e=(ArrayList<CategorieFiche>) query.getResultList();
			
			return e;
		}

	@Override
	public ArrayList<Etudiant> AfficherListEtudiantParClasse(int idsite,int year) {
		Query query = em.createQuery(
				"SELECT e FROM etudiant e JOIN e.departements  d join d.site s  join e.classe c join c.annee y"
				+ "  WHERE s.id=:idsite and y.Annee=:year  order by e.classe" ,
				Etudiant.class);
		query.setParameter("idsite", idsite);
		query.setParameter("year", year);
		 System.out.println(query.getResultList());
		ArrayList<Etudiant> e =new ArrayList<>();
		
		e=(ArrayList<Etudiant>) query.getResultList();
		
		return e;
	}

	@Override
	public ArrayList<Etudiant> AfficherListEtudiantParOption(int idsite,int year) {
		Query query = em.createQuery(
				"SELECT e FROM etudiant e JOIN e.departements  d join d.site s join e.classe c join c.options o join c.annee y"
				+ "  WHERE s.id=:idsite and y.Annee=:year  order by o" ,
				Etudiant.class);
		query.setParameter("idsite", idsite);
		query.setParameter("year", year);
		 System.out.println(query.getResultList());
		ArrayList<Etudiant> e =new ArrayList<>();
		
		e=(ArrayList<Etudiant>) query.getResultList();
		
		return e;
	}

	@Override
	public ArrayList<Etudiant> AfficherListEtudiantPardepartement(int idsite,int year) {
		Query query = em.createQuery(
				"SELECT e FROM etudiant e JOIN e.departements  d join d.site s  join e.classe c join c.annee y  "
				+ "  WHERE s.id=:idsite and y.Annee=:year order by e.departements" ,
				Etudiant.class);
		query.setParameter("idsite", idsite);
		query.setParameter("year", year);
		 System.out.println(query.getResultList());
		ArrayList<Etudiant> e =new ArrayList<>();
		
		e=(ArrayList<Etudiant>) query.getResultList();
		
		return e;
	}

	@Override
	public double PourcentageStageEtranger2(String tunisie, int annee) {
		TypedQuery<Long> query = em.createQuery(
				"SELECT  COUNT(*)  FROM etudiant e JOIN e.entreprise  en  join e.classe c join c.annee y"
				+ "  WHERE en.Pays=:tunisie and y.Annee=:annee "
				,Long.class);
		query.setParameter("tunisie", tunisie);
		query.setParameter("annee", annee);
		TypedQuery<Long> query1 = em.createQuery(
				"select count (* )from etudiant e  join e.classe c join c.annee y where y.Annee=:annee ",
				Long.class);
		query1.setParameter("annee", annee);
		//query.setParameter("employeid", employeId);
		double a =(1-((float)query.getSingleResult()/(float)query1.getSingleResult()))*100;
		return a;
	}

	@Override
	public ArrayList<Integer> getAllannee() {
		Query query = em.createQuery(
				"SELECT e.Annee FROM AnneeUniversitaire e " ,
				Integer.class);
		
		 System.out.println(query.getResultList());
		ArrayList<Integer> e =new ArrayList<>();
		
		e=(ArrayList<Integer>) query.getResultList();
		
		return e;
		
	}

	@Override
	public ArrayList<String> getAllPays() {
		Query query = em.createQuery(
				"SELECT e.Pays FROM Entreprise e group by e.Pays" ,
				String.class);
		
		 System.out.println(query.getResultList());
		ArrayList<String> e =new ArrayList<>();
		
		e=(ArrayList<String>) query.getResultList();
		
		return e;
		
		
	}

	@Override
	public double PourcentageStageEtranger1(String pays, int annee) {
		TypedQuery<Long> query = em.createQuery(
				/*"SELECT  COUNT(*)  FROM Entreprise en  "
				+ "  WHERE en.Pays=:pays "
				,Long.class);*/
				"SELECT  COUNT(*)  FROM etudiant e join e.entreprise en join e.classe c join c.annee y "
				+ "  WHERE en.Pays=:pays and y.Annee=:annee "
				,Long.class);
		query.setParameter("pays", pays);
		query.setParameter("annee", annee);
		/*TypedQuery<Long> query1 = em.createQuery(
				"select count (* )from etudiant  ",
				Long.class);*/
				TypedQuery<Long> query1 = em.createQuery(
				"select count (* )from etudiant e  join e.classe c join c.annee y where y.Annee=:annee ",
				Long.class);
		query1.setParameter("annee", annee);
		//query.setParameter("employeid", employeId);
		double a =((float)query.getSingleResult()/(float)query1.getSingleResult())*100;
		
		return a;
	}

	@Override
	public ArrayList<Etudiant> AfficherListEtudiantparannée(int idsite, int year) {
		Query query = em.createQuery(
				"SELECT e FROM etudiant e JOIN e.departements  d join d.site s join  e.classe c join c.annee y"
				+ "  WHERE s.id=:idsite and y.Annee=:year",
				Etudiant.class);
		query.setParameter("idsite", idsite);
		query.setParameter("year", year);
		 System.out.println(query.getResultList());
		ArrayList<Etudiant> e =new ArrayList<>();
		
		e=(ArrayList<Etudiant>) query.getResultList();
		
		return e;
	}

	@Override
	public ArrayList<Object[]> listerentrepriseavecordre() {
		Query query = em.createQuery(
				//"SELECT  count(*) catégories FROM fichepfe Catégories group by catégories"
				/*"select entreprise.NomEntreprise as e ,  count(*) as n  FROM etudiant et group by et.entreprise order by n  desc  union select etu from etudiant etu  join etu.departements d join "
				+ "d.site s join s.universite u "
				+ "  WHERE u.IdUnivetsity=1 "
				);*/
				"select entreprise.NomEntreprise as e ,  count(*) as n  FROM etudiant et where et.departements.site.universite.Nom='esprit' group by et.entreprise order by n  desc  "
				
				);
		
		 System.out.println(query.getResultList());
		 
		
		 ArrayList<Object[]> e =new ArrayList<>();
			
			e=(ArrayList<Object[]>) query.getResultList();
		
		return e;
	}

	@Override
	public long Pourcentagecatégoriestage(String categorie) {
		Query query = em.createQuery(
				//"SELECT  count(*) catégories FROM fichepfe Catégories group by catégories"
				"select   count(*) as n  FROM etudiant e join e.fiche f where f.Catégories=:categorie "
				,Long.class);
		query.setParameter("categorie", categorie);
		//query.setParameter("annee", annee);
		
		 System.out.println(query.getResultList());
		
		 long e = query.getFirstResult();
		
		return e;
	}

	@Override
	public ArrayList<String> ShowallCatégories() {
		Query query = em.createQuery(
				"SELECT f.Catégories FROM fichepfe f group by  f.Catégories" ,
				String.class);
		
		 System.out.println(query.getResultList());
		ArrayList<String> e =new ArrayList<>();
		
		e=(ArrayList<String>) query.getResultList();
		
		return e;
	}

	@Override
	public ArrayList<Object[]> listercatégorieavec(int annee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Etudiant> RechercheEtudiant(int idsite, int year, String rech) {
		Query query = em.createQuery(
				"SELECT e FROM etudiant e JOIN e.departements  d join d.site s join  e.classe c join c.annee y"
				+ "  WHERE s.id=:idsite and  e.Nom  like :rech or e.Prenom like :rech or e.classe.NomClass like :rech  and y.Annee=:year",
				Etudiant.class);
		query.setParameter("idsite", idsite);
		query.setParameter("year", year);
		query.setParameter("rech", "%"+rech+"%");
		 System.out.println(query.getResultList());
		ArrayList<Etudiant> e =new ArrayList<>();
		
		e=(ArrayList<Etudiant>) query.getResultList();
		
		return e;
	}
	@Override
	public void SaveFile(String content, File file){
	    try {
	        FileWriter fileWriter = null;
	         
	        fileWriter = new FileWriter(file);
	        fileWriter.write(content);
	        fileWriter.close();
	    } catch (IOException ex) {
	    	
	    	System.out.println(ex);
	        
	    }
		
	}

	@Override
	public String htmltoxhtml(String aa) {
		// TODO Auto-generated method stub
		/*Tidy tidy = new Tidy();
        // C'est là où on indique qu'on veut du XHTML en sortie
        tidy.setXHTML(true);
        try {
          // This is where the magic happens !!
          // La méthode parse permet de prendre une InputStream (en entrée)
          // et d'écrire une OutputStream en sortie
          tidy.parse(
               new FileInputStream("C://Users//zied//workspace//ProjetPFE//ProjetPFE-web//src//main//webapp//PageTemplatePFE//New.html"),
               new FileOutputStream("C://Users//zied//workspace//ProjetPFE//ProjetPFE-web//src//main//webapp//PageTemplatePFE//NewFile.xhtml"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println(e);
        }*/
		 Document document = Jsoup.parse(aa);
		    document.outputSettings().syntax(Document.OutputSettings.Syntax.xml);    
		   return document.html();
    }

	@Override
	public Etudiant getEtudiantWithId(int id) {
		Query query = em.createQuery(
				"SELECT e FROM etudiant e "
				+ "  WHERE e.id=:id",
				Etudiant.class);
		query.setParameter("id", id);
		 System.out.println(query.getResultList());
		Etudiant e =new Etudiant();
		
		e=(Etudiant) query.getSingleResult();
		
		return e;
	}
	@Override
	public ArrayList<Object[]>statcatégories(int Annee) {
		Query query = em.createQuery(
				//"SELECT  count(*) catégories FROM fichepfe Catégories group by catégories"
				"select Catégories as c ,  count(*) as n  FROM fichepfe f  where f.year.Annee=:Annee group by f.Catégories order by n  desc"
				);
			query.setParameter("Annee", Annee);
		 System.out.println(query.getResultList());
		
		 ArrayList<Object[]> e =new ArrayList<>();
			
			e=(ArrayList<Object[]>) query.getResultList();
		
		return e;
	}

	@Override
	public Long nombredeficheparcatégorieparannee(String catégorie, int annee) {
		// TODO Auto-generated method stub
		TypedQuery<Long> query = em.createQuery(
				//"SELECT  count(*) catégories FROM fichepfe Catégories group by catégories"
				"select count(*)  FROM fichepfe f  join f.year y where y.Annee=:annee and f.Catégories=:catégorie "
				,Long.class);
			query.setParameter("annee", annee);
			query.setParameter("catégorie", catégorie);
		 //System.out.println(query.getResultList());
		
		 Long e =(long) query.getSingleResult();
			
			
		
		return e;
	}
	@Override
	public double prourcentagereuisite(int annee){
	
		TypedQuery<Long> query = em.createQuery(
				"SELECT  COUNT(*)  FROM fichepfe f where f.note >=10 and f.year.Annee=:annee "
				,Long.class);
		
		query.setParameter("annee", annee);
		TypedQuery<Long> query1 = em.createQuery(
				"select count (* )from fichepfe f   where f.year.Annee=:annee ",
				Long.class);
		query1.setParameter("annee", annee);
		
		double a =((float)query.getSingleResult()/(float)query1.getSingleResult())*100;
		return a;
	}
		
	}


	


