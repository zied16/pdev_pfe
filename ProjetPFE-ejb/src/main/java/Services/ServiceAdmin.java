package Services;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import Entity.Departement;
import Entity.Employe;
import Entity.Etudiant;
import Entity.Employe.Role;
import Entity.Site;
import Interface.AdminRemote;
@Stateless
public class ServiceAdmin implements AdminRemote {

	@PersistenceContext
	EntityManager em;
	@Override
	public  int AjouterDepartement(Departement D) {
		em.persist(D);
		return D.getIddepartement();
	}
	@Override
	public void addEmploye(Employe Emp) {
		
		em.persist(Emp);
		}
	@Override
	public void AffecterDirDeStage(int adminId,int dirStageId,int siteId) {
		Employe admin = em.find(Employe.class,adminId);
		Site s=em.find(Site.class,siteId);
		if(!admin.getRole().equals(Role.Administarteur))
		{
			System.out.println("vous devez etre un adminstrateur");
			return;
		}
		//if(s.getDirDeStage()!= null)
		{
			System.out.println("ce Site a deja un directeur de stage");
			return;
		}
		
		//Employe dir = em.find(Employe.class,dirStageId);
			//s.setDirDeStage(dir);
			//dir.setRole(Role.DirecteurDeStage);
		
		
		
	}
	@Override
	public int AjouterSite(Site S) {
		em.persist(S);
		return S.getIdSite();
	}
	@Override
	public ArrayList<Employe> ReadAll() {
		Query query = em.createQuery(
				"SELECT e FROM Employe e "
				,
				Employe.class);
		
		 System.out.println(query.getResultList());
		ArrayList<Employe> e =new ArrayList<>();
		
		e=(ArrayList<Employe>) query.getResultList();
		
		return e;
	}
	@Override
	public ArrayList<String> ReadAllNomDepartement(int idsite) {
		Query query = em.createQuery(
				"SELECT d.Spécialité FROM Departement d join d.site s where s.id=:id  union  "+
				"SELECT e.departement.Spécialité FROM Employe e join e.departement d join d.site ss where ss.id=:id  "
				);
		query.setParameter("id", idsite);
		ArrayList<String> e =new ArrayList<>();
		
		e=(ArrayList<String>) query.getResultList();
		
		return e;
		
		
	}
	

}
