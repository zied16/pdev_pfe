package Services;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Entity.Employe;
import Interface.LoginRemote;

@Stateless
public class LoginService implements LoginRemote {
	@PersistenceContext
	EntityManager em;
	@Override
	public ArrayList<Employe> ReadAll() {
		Query query = em.createQuery(
				"SELECT emp FROM Employe emp " ,
				Employe.class);
		
		 System.out.println(query.getResultList());
		ArrayList<Employe> e =new ArrayList<>();
		
		e=(ArrayList<Employe>) query.getResultList();
		
		return e;
		
	}}
