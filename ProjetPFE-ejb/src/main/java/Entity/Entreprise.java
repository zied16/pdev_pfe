package Entity;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Entreprise
 *
 */
@Entity

public class Entreprise implements Serializable {

	   
	@Id
	private int IdEntreprise;
	private String NomEntreprise;
	private String NomResponsable;
	private String NomEncadrant;
	private String EmailEncadrant;
	private String Adresse;
	private String SiteWeb;
	private String Pays;
	private String tel;
	@OneToMany(mappedBy="entreprise")
	private List<Etudiant> etudiant;
	private static final long serialVersionUID = 1L;

	public Entreprise() {
		super();
	}   
	public int getIdEntreprise() {
		return this.IdEntreprise;
	}

	public void setIdEntreprise(int IdEntreprise) {
		this.IdEntreprise = IdEntreprise;
	}   
	public String getNomEntreprise() {
		return this.NomEntreprise;
	}

	public void setNomEntreprise(String NomEntreprise) {
		this.NomEntreprise = NomEntreprise;
	}   
	public String getNomResponsable() {
		return this.NomResponsable;
	}

	public void setNomResponsable(String NomResponsable) {
		this.NomResponsable = NomResponsable;
	}   
	public String getNomEncadrant() {
		return this.NomEncadrant;
	}

	public void setNomEncadrant(String NomEncadrant) {
		this.NomEncadrant = NomEncadrant;
	}   
	public String getEmailEncadrant() {
		return this.EmailEncadrant;
	}

	public void setEmailEncadrant(String EmailEncadrant) {
		this.EmailEncadrant = EmailEncadrant;
	}
   
}
