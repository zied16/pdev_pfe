package Entity;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Classe
 *
 */
@Entity

public class Classe implements Serializable {

	   
	@Id
	private int IdClass;
	private String NomClass;
	@ManyToOne
	private Option option;
	private static final long serialVersionUID = 1L;

	public Classe() {
		super();
	}   
	public int getIdClass() {
		return this.IdClass;
	}

	public void setIdClass(int IdClass) {
		this.IdClass = IdClass;
	}   
	public String getNomClass() {
		return this.NomClass;
	}

	public void setNomClass(String NomClass) {
		this.NomClass = NomClass;
	}
   
}
