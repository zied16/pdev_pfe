package Entity;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Departement
 *
 */
@Entity

public class Departement implements Serializable {

	   
	@Id
	private int Iddepartement;
	private String Spécialité;
	@OneToMany(mappedBy="departement")
	private List<Employe> emp;
	@ManyToOne
	private Site site;
	@OneToMany(mappedBy="depart")
	private List<Option> options;
	private Employe ChefDepartement;
	private static final long serialVersionUID = 1L;

	public Departement() {
		super();
	}   
	public int getIddepartement() {
		return this.Iddepartement;
	}

	public void setIddepartement(int Iddepartement) {
		this.Iddepartement = Iddepartement;
	}   
	public String getSpécialité() {
		return this.Spécialité;
	}

	public void setSpécialité(String Spécialité) {
		this.Spécialité = Spécialité;
	}
   
}
