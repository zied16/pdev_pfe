package Entity;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: FichePFE
 *
 */
@Entity

public class FichePFE implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)

	private int Id;
	private String Titre;
	private String Description;
	private boolean EstTraité;
	private boolean Refus;
	private String Motif;
	private String Catégories;
	private String Problématique;
	private String Tacheafaire;
	private boolean fait;
	private String Rapport;
	public String getTacheafaire() {
		return Tacheafaire;
	}
	public void setTacheafaire(String tacheafaire) {
		Tacheafaire = tacheafaire;
	}
	public boolean isFait() {
		return fait;
	}
	public void setFait(boolean fait) {
		this.fait = fait;
	}
	@ManyToOne
	private Employe Prevalidateur;
	@ManyToOne
	private Employe Encadrant;
	@ManyToOne
	private Employe Rapporteur;
	@OneToOne(mappedBy="fiche")
	private Etudiant E;
	@ManyToOne
	private TemplateFiche template;
	@ManyToOne
	private AnneeUniversitaire year;
	@ManyToMany (mappedBy="fiches")
	private List<Categorie> categories;
	private float Note;
	private String mention;
	
	public String getMention() {
		return mention;
	}
	public void setMention(String mention) {
		this.mention = mention;
	}
	public float getNote() {
		return Note;
	}
	public void setNote(float note) {
		Note = note;
	}
	private static final long serialVersionUID = 1L;

	public FichePFE() {
		super();
	}   
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}   
	public String getTitre() {
		return this.Titre;
	}

	public Employe getPrevalidateur() {
		return Prevalidateur;
	}
	public void setPrevalidateur(Employe prevalidateur) {
		Prevalidateur = prevalidateur;
	}
	public void setTitre(String Titre) {
		this.Titre = Titre;
	}   
	public boolean isRefus() {
		return Refus;
	}
	public void setRefus(boolean refus) {
		Refus = refus;
	}
	public String getMotif() {
		return Motif;
	}
	public void setMotif(String motif) {
		Motif = motif;
	}
	public String getDescription() {
		return this.Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}   
	public boolean getEstTraité() {
		return this.EstTraité;
	}

	public void setEstTraité(boolean EstTraité) {
		this.EstTraité = EstTraité;
	}   
	public String getCatégories() {
		return this.Catégories;
	}

	public void setCatégories(String Catégories) {
		this.Catégories = Catégories;
	}   
	public String getProblématique() {
		return this.Problématique;
	}

	public void setProblématique(String Problématique) {
		this.Problématique = Problématique;
	}
	public Employe getEncadrant() {
		return Encadrant;
	}
	public void setEncadrant(Employe encadrant) {
		Encadrant = encadrant;
	}
	public Employe getRapporteur() {
		return Rapporteur;
	}
	public void setRapporteur(Employe rapporteur) {
		Rapporteur = rapporteur;
	}
	public Etudiant getE() {
		return E;
	}
	public void setE(Etudiant e) {
		E = e;
	}
	public TemplateFiche getTemplate() {
		return template;
	}
	public void setTemplate(TemplateFiche template) {
		this.template = template;
	}
	public AnneeUniversitaire getYear() {
		return year;
	}
	public void setYear(AnneeUniversitaire year) {
		this.year = year;
	}
	public List<Categorie> getCategories() {
		return categories;
	}
	public void setCategories(List<Categorie> categories) {
		this.categories = categories;
	}
   
}
