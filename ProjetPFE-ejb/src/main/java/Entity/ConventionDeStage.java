package Entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: ConventionDeStage
 *
 */
@Entity

public class ConventionDeStage implements Serializable {

	   
	@Id
	private int Id;
	private Etudiant etu;
	private Entreprise entreprise;
	private Employe emp;
	private static final long serialVersionUID = 1L;

	public ConventionDeStage() {
		super();
	}   
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}
   
}
