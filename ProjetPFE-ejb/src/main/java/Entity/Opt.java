package Entity;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Option
 *
 */
@Entity

public class Opt implements Serializable {

	   
	@Id
	private int Idopt;
	private String Nomoption;
	@ManyToOne
	private Departement depart;
	@OneToMany(mappedBy="option")
	private List<Classe> classe;
	private static final long serialVersionUID = 1L;

	public Opt() {
		super();
	}   
	public int getIdopt() {
		return this.Idopt;
	}

	public void setIdopt(int Idopt) {
		this.Idopt = Idopt;
	}   
	public String getNomoption() {
		return this.Nomoption;
	}

	public void setNomoption(String Nomoption) {
		this.Nomoption = Nomoption;
	}
   
}
