package Entity;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Employe
 *
 */
@Entity

public class Employe implements Serializable {
	
	public enum Role {ChefDepartement,Administarteur,Enseignant,DirecteurDeStage,SuperAdmin}
	   
	@Id
	

	private int Id;
	private String Nom;
	private String Prenom;
	private String Email;
	private int Tel;
	private String username;
	@OneToOne(mappedBy="etudiant")
	private Soutenance soutenance;
	@OneToOne(mappedBy="employe")
	private Soutenance soutenance1;
	public Soutenance getSoutenance() {
		return soutenance;
	}

	public void setSoutenance(Soutenance soutenance) {
		this.soutenance = soutenance;
	}

	public Soutenance getSoutenance1() {
		return soutenance1;
	}

	public void setSoutenance1(Soutenance soutenance1) {
		this.soutenance1 = soutenance1;
	}

	private String password;
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@ManyToOne
	private Departement departement;
	@OneToMany(mappedBy="emp")
	private List<ChoixCategorie> categories;
	private Role role;
	@OneToMany(mappedBy="Rapporteur")
	private List<FichePFE> Fichearapporter;
	@OneToMany(mappedBy="Encadrant")
	private List<FichePFE> Ficheaencadrer;
	@OneToMany(mappedBy="Prevalidateur")
	private List<FichePFE> Ficheaprévalider;
	
	
	private static final long serialVersionUID = 1L;

	public Employe() {
		super();
		this.categories=new ArrayList();
	}   
	
	public List<FichePFE> getFicheaprévalider() {
		return Ficheaprévalider;
	}

	public void setFicheaprévalider(List<FichePFE> ficheaprévalider) {
		Ficheaprévalider = ficheaprévalider;
	}

	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}   
	public String getNom() {
		return this.Nom;
	}

	public void setNom(String Nom) {
		this.Nom = Nom;
	}   
	public String getPrenom() {
		return this.Prenom;
	}

	public void setPrenom(String Prenom) {
		this.Prenom = Prenom;
	}   
	public String getEmail() {
		return this.Email;
	}

	public void setEmail(String Email) {
		this.Email = Email;
	}   
	public int getTel() {
		return this.Tel;
	}

	public void setTel(int Tel) {
		this.Tel = Tel;
	}
	public Departement getDepartement() {
		return departement;
	}
	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public List<ChoixCategorie> getCategories() {
		return categories;
	}

	public void setCategories(List<ChoixCategorie> categories) {
		this.categories = categories;
	}

	public void assignCategorieToEmployee(List<ChoixCategorie>category){
		this.setCategories(category);
		for(ChoixCategorie c : category){
			c.setEmp(this);
			
		}
	}

	public List<FichePFE> getFichearapporter() {
		return Fichearapporter;
	}

	public void setFichearapporter(List<FichePFE> fichearapporter) {
		Fichearapporter = fichearapporter;
	}

	public List<FichePFE> getFicheaencadrer() {
		return Ficheaencadrer;
	}

	public void setFicheaencadrer(List<FichePFE> ficheaencadrer) {
		Ficheaencadrer = ficheaencadrer;
	}	
	
   
}
