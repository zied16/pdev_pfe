package Entity;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Etudiant
 *
 */
@Entity

public class Etudiant implements Serializable {

	   
	@Id
	private int Id;
	private String Nom;
	private String Prenom;
	private String Email;
	private boolean isValid;
	private Employe Encadreur;
	private Employe Rapporteur;
	private String username;
	private String password;
	@OneToOne(mappedBy="etudiant")
	private Soutenance soutenance;
	@OneToOne
	private FichePFE fiche;
	@ManyToOne
	private Entreprise entreprise;
	private static final long serialVersionUID = 1L;

	public Etudiant() {
		super();
	}   
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}   
	public String getNom() {
		return this.Nom;
	}

	public void setNom(String Nom) {
		this.Nom = Nom;
	}   
	public String getPrenom() {
		return this.Prenom;
	}

	public void setPrenom(String Prenom) {
		this.Prenom = Prenom;
	}   
	public String getEmail() {
		return this.Email;
	}

	public void setEmail(String Email) {
		this.Email = Email;
	}   
	public boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}
   
}
