package Entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class ChoixCategorie implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
@EmbeddedId	
private ChoixCategoriePK IdChoix;
@ManyToOne
@JoinColumn(insertable= false,updatable= false,name="IdEmploye",referencedColumnName="Id")
private Employe emp;
@ManyToOne
@JoinColumn(insertable= false,updatable= false,name="IdCategorie",referencedColumnName="IdCat")
private Categorie cat;

public ChoixCategoriePK getIdChoix() {
	return IdChoix;
}
public void setIdChoix(ChoixCategoriePK idChoix) {
	IdChoix = idChoix;
}
public Employe getEmp() {
	return emp;
}
public void setEmp(Employe emp) {
	this.emp = emp;
}
public Categorie getCat() {
	return cat;
}
public void setCat(Categorie cat) {
	this.cat = cat;
}


}



