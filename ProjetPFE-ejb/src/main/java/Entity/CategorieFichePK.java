package Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class CategorieFichePK implements Serializable{

	
	public int getIdCategorie() {
		return IdCategorie;
	}
	public void setIdCategorie(int idCategorie) {
		IdCategorie = idCategorie;
	}
	public int getIdfichee() {
		return IdFiche;
	}
	public void setIdEmploye(int idfiche) {
		IdFiche = idfiche;
	}
	private static final long serialVersionUID = 1L;

	//@Column(name = "Idc")
	private int IdCategorie;
	//@Column(name = "Id")
	private int IdFiche;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + IdCategorie;
		result = prime * result + IdFiche;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CategorieFichePK other = (CategorieFichePK) obj;
		if (IdCategorie != other.IdCategorie)
			return false;
		if (IdFiche != other.IdFiche)
			return false;
		return true;
	}
	
	
	

}
