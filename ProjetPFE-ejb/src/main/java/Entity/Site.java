package Entity;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Site
 *
 */
@Entity

public class Site implements Serializable {

	   
	@Id
	private int IdSite;
	private String Lieu;
	@OneToMany(mappedBy="site")
	private List<Departement> dep;
	@ManyToOne
	private University universite;
	private static final long serialVersionUID = 1L;

	public Site() {
		super();
	}   
	public int getIdSite() {
		return this.IdSite;
	}

	public void setIdSite(int IdSite) {
		this.IdSite = IdSite;
	}   
	public String getLieu() {
		return this.Lieu;
	}

	public void setLieu(String Lieu) {
		this.Lieu = Lieu;
	}
   
}
