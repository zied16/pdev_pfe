package Entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ChoixCategoriePK implements Serializable{

	private static final long serialVersionUID = 1L;

	
	private int IdCategorie;
	private int IdEmploye;
	public int getIdCategorie() {
		return IdCategorie;
	}
	public void setIdCategorie(int idCategorie) {
		IdCategorie = idCategorie;
	}
	public int getIdEmploye() {
		return IdEmploye;
	}
	public void setIdEmploye(int idEmploye) {
		IdEmploye = idEmploye;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + IdCategorie;
		result = prime * result + IdEmploye;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChoixCategoriePK other = (ChoixCategoriePK) obj;
		if (IdCategorie != other.IdCategorie)
			return false;
		if (IdEmploye != other.IdEmploye)
			return false;
		return true;
	}
	
	
	
	
	
	
}
