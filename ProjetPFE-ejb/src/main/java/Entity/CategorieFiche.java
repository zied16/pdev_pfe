package Entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class CategorieFiche implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EmbeddedId	
	private CategorieFichePK IdCategoriefiche;
	@ManyToOne
	@JoinColumn(insertable= false,updatable= false,name="IdFiche",referencedColumnName="Id")
	private FichePFE fic;
	@ManyToOne
	@JoinColumn(insertable= false,updatable= false,name="IdCategorie",referencedColumnName="IdCat")
	private Categorie cate;
	
	
	
	public CategorieFiche() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CategorieFichePK getIdCategoriefiche() {
		
	
		return IdCategoriefiche;
	}
	public void setIdCategoriefiche(CategorieFichePK idCategoriefiche) {
		IdCategoriefiche = idCategoriefiche;
	}
	public FichePFE getFic() {
		return fic;
	}
	public void setFic(FichePFE fic) {
		this.fic = fic;
	}
	public Categorie getCate() {
		return cate;
	}
	public void setCate(Categorie cate) {
		this.cate = cate;
	}
	/*public CategorieFiche( FichePFE fic, Categorie cate) {
		this.getIdCategoriefiche().setIdCategorie(cate.getIdCat());
		this.getIdCategoriefiche().setIdEmploye(fic.getId());
		this.fic = fic;
		this.cate = cate;
	}
	*/

}
