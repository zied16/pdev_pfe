package Entity;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: University
 *
 */
@Entity

public class University implements Serializable {

	
	private String Nom;
	@Id
	private int IdUnivetsity;
	@OneToMany(mappedBy="universite")
	private List<Site> site;
	private Employe.Role admin;
	
	
	private static final long serialVersionUID = 1L;

	public University() {
		super();
	}   
	public String getNom() {
		return this.Nom;
	}

	public void setNom(String Nom) {
		this.Nom = Nom;
	}   
	public int getIdUnivetsity() {
		return this.IdUnivetsity;
	}

	public void setIdUnivetsity(int IdUnivetsity) {
		this.IdUnivetsity = IdUnivetsity;
	}
   
}
