package Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * Entity implementation class for Entity: AnneeUniversitaire
 *
 */
@Entity


public class AnneeUniversitaire implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)

	private int Annee;
	@OneToMany(mappedBy="year")
	private List<FichePFE> fichet;
	private static final long serialVersionUID = 1L;

	public AnneeUniversitaire() {
		super();
	}   
	public int getAnnee() {
		return this.Annee;
	}

	public void setAnnee(int Annee) {
		this.Annee = Annee;
	}
   
}
