package Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: TemplateFiche
 *
 */
@Entity

public class TemplateFiche implements Serializable {

	   
	@Id
	private int Id;
	@OneToMany(mappedBy="template")
	private List<FichePFE> fichepfe;
	private static final long serialVersionUID = 1L;

	public TemplateFiche() {
		super();
	}   
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}
   
}
