package Entity;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Categorie
 *
 */
@Entity

public class Categorie implements Serializable {

	   
	@Id
	
	private int IdCat;
	
	private String NomCategory;
	private static final long serialVersionUID = 1L;
	@ManyToMany
    private List<FichePFE> fiches;
	@OneToMany(mappedBy="cat")
	private List<ChoixCategorie> enseignents;
	private boolean etat;
	public Categorie() {
		super();
		this.enseignents=new ArrayList();
	}   
	public List<FichePFE> getFiches() {
		return fiches;
	}
	public void setFiches(List<FichePFE> fiches) {
		this.fiches = fiches;
	}
	
	public boolean isEtat() {
		return etat;
	}
	public List<ChoixCategorie> getEnseignents() {
		return enseignents;
	}
	public void setEnseignents(List<ChoixCategorie> enseignents) {
		this.enseignents = enseignents;
	}
	public void setEtat(boolean etat) {
		this.etat = etat;
	}
	
	public int getIdCat() {
		return IdCat;
	}
	public void setIdcat(int idCat) {
		IdCat = idCat;
	}
	public String getNomCategory() {
		return this.NomCategory;
	}

	public void setNomCategory(String NomCategory) {
		this.NomCategory = NomCategory;
	}
	public void assignEmployeeToCategory(List<ChoixCategorie>category){
		this.setEnseignents(category);
		for(ChoixCategorie c : category){
			c.setCat(this);
			
		}
	
}}