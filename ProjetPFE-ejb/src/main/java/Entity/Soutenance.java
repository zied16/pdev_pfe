package Entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Soutenance
 *
 */
@Entity

public class Soutenance implements Serializable {

	   
	@Id
	@GeneratedValue
	private int Id;
	Date DateSoutenance;
	@OneToOne
	private Etudiant etudiant;
	@OneToOne
	private Employe employe;

	public Employe getEmploye() {
		return employe;
	}
	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	private String titre;
	public Date getDateSoutenance() {
		return DateSoutenance;
	}
	public void setDateSoutenance(Date dateSoutenance) {
		DateSoutenance = dateSoutenance;
	}
	public Etudiant getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}

	private static final long serialVersionUID = 1L;

	public Soutenance() {
		super();
	}   
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}
   
}
