package Interface;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Remote;

import Entity.AnneeUniversitaire;
import Entity.Categorie;
import Entity.CategorieFiche;
import Entity.Entreprise;
import Entity.Etudiant;
import Entity.FichePFE;
import Entity.University;

@Remote
public interface DirecteurStageRemote {
	public ArrayList<Etudiant> AfficherListEtudiant(int idsite);
	public double PourcentageStageEtranger(String tunisie);
	public double PourcentageStageEtranger1(String pays);
	public long Nombreetudiantparsite(int idsite);
	public  ArrayList<Etudiant>listentrepriseetudiantesprit(String esprit);
	public ArrayList<FichePFE>listercatégorie();
	public ArrayList<Object[]>listercatégorieavec();
	public ArrayList<Object[]>listerentrepriseavecordre();
	public double evolutionPourcentageStageEtranger(String tunisie,int year);
	public long Nombreetudiantparsiteetparannee(int idsite,int year);
	public void ajouterCategoriefiche(int idfiche,int idCategorie);
	public ArrayList< CategorieFiche>listerficheavecsescatégorie();
	public ArrayList<Etudiant> AfficherListEtudiantParClasse(int idsite,int year);
	public ArrayList<Etudiant> AfficherListEtudiantParOption(int idsite,int year);
	public ArrayList<Etudiant> AfficherListEtudiantPardepartement(int idsite,int year);
	public double PourcentageStageEtranger2(String tunisie,int annee);
	public ArrayList<Integer>getAllannee();
	public ArrayList<String>getAllPays();
	public double PourcentageStageEtranger1(String pays,int annee);
	public ArrayList<Etudiant> AfficherListEtudiantparannée(int idsite,int year);
	public long Pourcentagecatégoriestage(String categorie);
	public ArrayList<String>ShowallCatégories();
	public ArrayList<Object[]>listercatégorieavec(int annee);
	
	public ArrayList<Etudiant> RechercheEtudiant(int idsite,int year,String rech); 
	
	//public void CreerTemplate(String Contenu);
	public void SaveFile(String content, File file);
	public String htmltoxhtml(String aa);
	
	public Etudiant getEtudiantWithId(int id);
	public ArrayList<Object[]>statcatégories(int Annee);
	
	public Long nombredeficheparcatégorieparannee(String catégorie,int annee);
	public double prourcentagereuisite(int annee);
	
	
	
	

}
