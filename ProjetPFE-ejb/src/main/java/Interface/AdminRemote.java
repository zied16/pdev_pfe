package Interface;

import java.util.ArrayList;

import javax.ejb.Remote;

import Entity.Departement;
import Entity.Employe;
import Entity.Site;

@Remote
public interface AdminRemote {
	public int AjouterSite(Site S);
	public  int AjouterDepartement(Departement D);
	public void addEmploye(Employe Emp);
	public void AffecterDirDeStage(int adminId,int chefdepId,int depId);
	public ArrayList<Employe>ReadAll();
	public ArrayList<String>ReadAllNomDepartement(int idsite);
	

}
