package Interface;

import java.util.ArrayList;

import javax.ejb.Remote;

import Entity.Employe;
@Remote
public interface LoginRemote {
	public ArrayList<Employe> ReadAll();
}
